DROP DATABASE IF EXISTS `ssm-crm`  ;
CREATE DATABASE `ssm-crm`;
USE `ssm-crm`;

-- ----------------------------
-- 数据字典表
-- ----------------------------
DROP TABLE IF EXISTS `base_dict`;
CREATE TABLE `base_dict` (
  `dict_id` varchar(32) NOT NULL COMMENT '数据字典id(主键)',
  `dict_type_code` varchar(10) NOT NULL COMMENT '数据字典类别代码',
  `dict_type_name` varchar(64) NOT NULL COMMENT '数据字典类别名称',
  `dict_item_name` varchar(64) NOT NULL COMMENT '数据字典项目名称',
  `dict_item_code` varchar(10) DEFAULT NULL COMMENT '数据字典项目代码(可为空)',
  `dict_sort` int(10) DEFAULT NULL COMMENT '排序字段',
  `dict_enable` char(1) NOT NULL COMMENT '1:使用 0:停用',
  `dict_memo` varchar(64) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- 数据字典表数据
-- ----------------------------
INSERT INTO `base_dict` VALUES ('1', '001', '客户行业', '教育培训 ', null, '1', '1', null);
INSERT INTO `base_dict` VALUES ('10', '003', '公司性质', '民企', null, '3', '1', null);
INSERT INTO `base_dict` VALUES ('12', '004', '年营业额', '1-10万', null, '1', '1', null);
INSERT INTO `base_dict` VALUES ('13', '004', '年营业额', '10-20万', null, '2', '1', null);
INSERT INTO `base_dict` VALUES ('14', '004', '年营业额', '20-50万', null, '3', '1', null);
INSERT INTO `base_dict` VALUES ('15', '004', '年营业额', '50-100万', null, '4', '1', null);
INSERT INTO `base_dict` VALUES ('16', '004', '年营业额', '100-500万', null, '5', '1', null);
INSERT INTO `base_dict` VALUES ('17', '004', '年营业额', '500-1000万', null, '6', '1', null);
INSERT INTO `base_dict` VALUES ('18', '005', '客户状态', '基础客户', null, '1', '1', null);
INSERT INTO `base_dict` VALUES ('19', '005', '客户状态', '潜在客户', null, '2', '1', null);
INSERT INTO `base_dict` VALUES ('2', '001', '客户行业', '电子商务', null, '2', '1', null);
INSERT INTO `base_dict` VALUES ('20', '005', '客户状态', '成功客户', null, '3', '1', null);
INSERT INTO `base_dict` VALUES ('21', '005', '客户状态', '无效客户', null, '4', '1', null);
INSERT INTO `base_dict` VALUES ('22', '006', '客户级别', '普通客户', null, '1', '1', null);
INSERT INTO `base_dict` VALUES ('23', '006', '客户级别', 'VIP客户', null, '2', '1', null);
INSERT INTO `base_dict` VALUES ('24', '007', '商机状态', '意向客户', null, '1', '1', null);
INSERT INTO `base_dict` VALUES ('25', '007', '商机状态', '初步沟通', null, '2', '1', null);
INSERT INTO `base_dict` VALUES ('26', '007', '商机状态', '深度沟通', null, '3', '1', null);
INSERT INTO `base_dict` VALUES ('27', '007', '商机状态', '签订合同', null, '4', '1', null);
INSERT INTO `base_dict` VALUES ('3', '001', '客户行业', '对外贸易', null, '3', '1', null);
INSERT INTO `base_dict` VALUES ('30', '008', '商机类型', '新业务', null, '1', '1', null);
INSERT INTO `base_dict` VALUES ('31', '008', '商机类型', '现有业务', null, '2', '1', null);
INSERT INTO `base_dict` VALUES ('32', '009', '商机来源', '电话营销', null, '1', '1', null);
INSERT INTO `base_dict` VALUES ('33', '009', '商机来源', '网络营销', null, '2', '1', null);
INSERT INTO `base_dict` VALUES ('34', '009', '商机来源', '推广活动', null, '3', '1', null);
INSERT INTO `base_dict` VALUES ('4', '001', '客户行业', '酒店旅游', null, '4', '1', null);
INSERT INTO `base_dict` VALUES ('5', '001', '客户行业', '房地产', null, '5', '1', null);
INSERT INTO `base_dict` VALUES ('6', '002', '客户信息来源', '电话营销', null, '1', '1', null);
INSERT INTO `base_dict` VALUES ('7', '002', '客户信息来源', '网络营销', null, '2', '1', null);
INSERT INTO `base_dict` VALUES ('8', '003', '公司性质', '合资', null, '1', '1', null);
INSERT INTO `base_dict` VALUES ('9', '003', '公司性质', '国企', null, '2', '1', null);

-- ----------------------------
-- 客户信息表
-- ----------------------------
CREATE TABLE `customer`(
	`cust_id` BIGINT  PRIMARY KEY AUTO_INCREMENT,
	`cust_name` VARCHAR(100) NOT NULL COMMENT '客户姓名',
	`cust_phonenum` VARCHAR(11) COMMENT '客户联系方式',
	`cust_gender` INT(1) COMMENT '客户性别',
	`cust_source` varchar(32) DEFAULT NULL COMMENT '客户信息来源',
	`cust_industry` varchar(32) DEFAULT NULL COMMENT '客户所属行业',
	`cust_level` varchar(32) DEFAULT NULL COMMENT '客户级别',
	`cust_address` VARCHAR(200) COMMENT '客户地址',
	`cust_picpath` VARCHAR(100) COMMENT '客户图片位置',
	`createdate` DATE COMMENT '客户信息创建时间'
)ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8;;


-- ----------------------------
-- 客户信息表 数据
-- ----------------------------
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(6,2,22,'马云','13687613109',0,'杭州',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(6,2,22,'马化腾','13687613109',0,'广州',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(6,2,22,'李彦宏','13687613109',0,'北京',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(6,2,22,'张龙','17823499999',0,'开封',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(6,2,22,'赵虎','13687243109',0,'汴京',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(6,2,22,'王朝','13687533109',0,'黑风寨',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(6,2,22,'马汉','13623313109',0,'花果山',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(6,2,22,'展昭','13652343109',0,'高老庄',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(6,2,22,'公孙胜','13535347109',0,'狮驼岭',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(6,2,22,'阮小七','13687609109',0,'流沙河',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(6,2,22,'阮小五','11187613109',0,'通天河',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(6,2,22,'戴宗','13245613109',0,'小西天',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(7,1,23,'武松','13687613554',0,'景阳冈',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(7,1,23,'卡莎','13687611234',1,'虚空',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(7,1,23,'薇恩','11234613109',1,'德玛西亚',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(7,1,23,'凯瑟琳','13623453109',1,'皮特沃夫',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(7,1,23,'厄运小姐','13687456709',1,'比尔吉沃特',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(7,1,23,'提莫','13687698769',0,'德玛西亚',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(7,1,23,'小炮','13681234109',1,'弗雷尔卓德',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(7,1,23,'亚索','13623453109',0,'无畏先锋',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(7,1,23,'飞机','13653423109',0,'扭曲丛林',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(7,1,23,'卢锡安','13132513109',0,'祖安',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(7,1,23,'千珏','13687346309',1,'黑色玫瑰',NULL,SYSDATE());
INSERT INTO `customer` (`cust_source`,`cust_industry`,`cust_level`,`cust_name`,`cust_phonenum`,`cust_gender`,`cust_address`,`cust_picpath`,`createdate`) VALUES(7,1,23,'希维尔','13687524309',1,'艾欧尼亚',NULL,SYSDATE());
                                                                               

